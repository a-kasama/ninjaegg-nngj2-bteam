﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameJam2_TeamB
{
	public class UIManager : MonoBehaviour {
		public GameObject resltUI;
		public GameObject headUI;

		public Text lifeText;
		public Text scoreText;
		public Text timeText;
		public Text gameOver;
		public Text timeOver;
		public Text ResultScoreNum;
		public Button playAgainButton;

		private bool clearFlag = false;

		void Start () {

		}

		// Update is called once per frame
		void Update () 
		{
			if (ManagerGame.data != null)
			{
				lifeText.text = ManagerGame.data.life.ToString();
				scoreText.text = ManagerGame.data.score.ToString();
				timeText.text = ((int)ManagerGame.data.timeLeft).ToString();

				if(ManagerGame.data.life == 0 && !clearFlag)
				{
					clearFlag = true;
					GameOver();
				}

				if(((int)ManagerGame.data.timeLeft) <= 0 && !clearFlag)
				{
					clearFlag = true;
					TimeOver();
				}
			}
		}

		public void TimeOver()
		{
			timeOver.gameObject.SetActive (true);
			GameResult ();
		}

		public void GameOver()
		{
			gameOver.gameObject.SetActive (true);
			GameResult ();
		}

		public void GameResult()
		{
			ResultScoreNum.text = ManagerGame.data.score.ToString ();
			resltUI.SetActive (true);
			headUI.SetActive (false);
		}

		public void OnStart()
		{
			ManagerGame.PlayAgain ();
		}
	}
}
