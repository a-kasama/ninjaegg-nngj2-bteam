﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameJam2_TeamB
{
	public class HeadUI : MonoBehaviour {
		public Text lifeText;
		public Text scoreText;
		public Text timeText;

		// Use this for initialization
		void Start () {

		}
		
		// Update is called once per frame
		void Update () 
		{
			if (ManagerGame.data != null)
			{
				lifeText.text = ManagerGame.data.life.ToString();
				scoreText.text = ManagerGame.data.score.ToString();
				timeText.text = ((int)ManagerGame.data.timeLeft).ToString();
			}

		}
	}
}
