﻿using UnityEngine;
using System.Collections;

namespace GameJam2_TeamB
{

	public class ComponentBackground : MonoBehaviour 
	{

		private float speedStart = 1f;
		private float speedEnd = 5f;
		private float timeEnd = 60f;
	
		private float timeStart;

		private float speed = -80;
		public float timeSpent;
		public float coef;

		Vector3 v3 = Vector3.zero;

		void Start ()
		{
			timeStart = Time.time;
			transform.eulerAngles = v3;
		}

		void Update()
		{
			Rotation ();
		}

		float timeleft;
		int loopNum;
		void Rotation()
		{
			timeleft -= Time.deltaTime;
			if(timeleft <= 0.0){
				timeleft = 5.0f;
				if(loopNum <= 5){
					speed = speed - 50;
					loopNum++;
				}else {
					speed = UnityEngine.Random.Range(-80, -350);
				}
			}

			v3.x += speed * Time.deltaTime;
			transform.eulerAngles = v3;

		}

	//	void aa()
	//	{
		//	Application.LoadLevelAdditive ("s_graphic");
		//	GameObject goBackgorund = ((ComponentBackground)FindObjectOfType (typeof(ComponentBackground))).gameObject;
	// 	}

	}
}