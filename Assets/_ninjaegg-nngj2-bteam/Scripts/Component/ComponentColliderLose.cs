﻿using UnityEngine;
using System.Collections;

namespace GameJam2_TeamB
{
	public class ComponentColliderLose : MonoBehaviour 
	{
		void OnTriggerEnter(Collider collider)
		{
			Debug.Log ("ComponentColliderLose.OnTriggerEnter(" + collider.gameObject.name + ")");
			ManagerGame.AddLife (-1);
		}
	}
}