﻿using UnityEngine;
using System.Collections;

namespace GameJam2_TeamB
{
	public class EntityBase : MonoBehaviour 
	{
		public TOKEI_TYPE type;

		public Animator animator;

		public virtual void Start () 
		{
		
		}
		
		public virtual void Update () 
		{
		
		}
	}
}
