﻿using UnityEngine;
using System.Collections;

namespace GameJam2_TeamB
{

	public class EntityApple : EntityBase
	{
		public TOKEI_STATE state;


		// DIRTY BECAUSE I AM SEXY! MOUAHAHAHAHAHAH

		private float jumpDuration = 0.5f;
		float jumpStart;

		private float hitDuration = 0.5f;
		float hitStart;

		override public void Start () 
		{
			Idle();
		}

		override public void Update () 
		{
			if (state != TOKEI_STATE.HIT &&
			    state != TOKEI_STATE.JUMP &&
			    Input.GetMouseButtonDown(0))
			{
				Jump ();
			}
			else if (state == TOKEI_STATE.JUMP &&
			         Time.time - jumpStart >= jumpDuration)
			{
				Idle();
			}
			else if (state == TOKEI_STATE.HIT &&
			         Time.time - hitDuration >= hitDuration)
			{
				Idle();
			}
		}

		public void Init()
		{
			Idle ();
		}

		public void Idle()
		{
			ForceAnimationState("forceIDLE");
			state = TOKEI_STATE.IDLE;
		}
		
		public void Hit()
		{
			ForceAnimationState("forceHIT");
			state = TOKEI_STATE.HIT;
			hitStart = Time.time;
		}
		
		public void Jump()
		{
			ForceAnimationState("forceJUMP");
			state = TOKEI_STATE.JUMP;
			jumpStart = Time.time;
		}

		public void Stop()
		{
			ForceAnimationState("forceSTOP");
			state = TOKEI_STATE.STOP;
			jumpStart = Time.time;
		}
		
		public void ForceAnimationState(string forceState)
		{		
			string [] states = {
				"forceJUMP",
				"forceIDLE",
				"forceSTOP",
				"forceHIT"
			};
			
			foreach(string state in states)
			{
				if (state != forceState)
					animator.SetBool(state, false);
			}
			
			animator.SetBool(forceState, true);
		}
	}
}