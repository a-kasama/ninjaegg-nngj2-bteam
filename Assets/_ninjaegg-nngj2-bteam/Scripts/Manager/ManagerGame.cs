﻿using UnityEngine;
using System.Collections;

namespace GameJam2_TeamB
{
	public class GameData 
	{
		public int score = 0;
		public int life = 3;
		public float timeLeft = 60;
	}

	public class ManagerGame : MonoBehaviour
	{
		private float timeStart;

		public void Start()
		{
			ManagerGame.New ();
		}

		public void Update()
		{
			data.timeLeft -= Time.deltaTime;

			if (data.timeLeft < 0)
			{
				ManagerGame.TimeOver();
				enabled = false;
			}
		}


		// ============================= //


		public static GameData data;

		public static void New () 
		{
			Debug.Log ("New()");
			data = new GameData ();
			((EntityApple)GameObject.FindObjectOfType (typeof(EntityApple))).Init ();
		}

		public static void AddLife(int count)
		{
			if (count < 0)
				((EntityApple)GameObject.FindObjectOfType (typeof(EntityApple))).Hit ();

			data.life += count;

			if (data.life <= 0)
				GameOver ();

			Debug.Log ("LIFE = " + data.life + ";");
		}

		public static void AddScore(int count)
		{
			if (data.life == 0)
				return;
			data.score += count;
			Debug.Log ("SCORE = " + data.score + ";");
		}

		public static void GameOver()
		{
			((EntityApple)GameObject.FindObjectOfType (typeof(EntityApple))).Stop ();
			// UI GAMEOVER
			Debug.Log ("GameOver()");
		}

		public static void TimeOver()
		{
			((EntityApple)GameObject.FindObjectOfType (typeof(EntityApple))).Stop ();
			// UI TIME OVER
			Debug.Log ("TimeOver()");
		}


		public static void PlayAgain()
		{

			Debug.Log ("PlayAgain()");
			Application.LoadLevel ("s_graphic");
		}


	}
}