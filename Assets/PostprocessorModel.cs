﻿using UnityEditor;

namespace PlatinumEgg
{
	public class PostprocessorModel : AssetPostprocessor
	{
		public void OnPreprocessModel()
		{
			ModelImporter model = (ModelImporter)assetImporter;

			model.importMaterials = false;
		//	model.animationType = ModelImporterAnimationType.Legacy;
		}
	}
}