//Maya ASCII 2015 scene
//Name: LVL_Planet_A.ma
//Last modified: Sat, May 16, 2015 12:56:20 PM
//Codeset: 1252
requires maya "2015";
currentUnit -l meter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201410051530-933320";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -8.787094658851613 5.0229575922007994 -4.7278974038602719 ;
	setAttr ".r" -type "double3" -21.088833640343164 240.37476558746854 0 ;
	setAttr ".rp" -type "double3" -1.4210854715202004e-016 0 5.6843418860808016e-016 ;
	setAttr ".rpt" -type "double3" 9.816186737329793e-017 1.0035983871552093e-015 -3.4265324191186408e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 10.692606626210493;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 0.0097774992719337206 19.809878600831492 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.10000000000001 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 100.10000000000001;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.10000000000001 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 100.10000000000001;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.10000000000001 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 100.10000000000001;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "LVL_Planet_";
createNode transform -n "A" -p "LVL_Planet_";
createNode transform -n "Planet" -p "A";
createNode transform -n "pSphere7" -p "Planet";
	setAttr ".s" -type "double3" 1 -1 1 ;
createNode mesh -n "pSphereShape1" -p "pSphere7";
	setAttr -k off ".v";
	setAttr -s 8 ".iog";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64709771743548206 0.44099964428851957 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 55 ".uvst[0].uvsp[0:54]" -type "float2" 0.7075457 0.78272414
		 0.68548375 0.7827242 0.66029721 0.7827242 0.63419873 0.78272426 0.60896659 0.7827242
		 0.58708918 0.7827242 0.76627171 0.69806808 0.72276747 0.69806808 0.67332971 0.69806808
		 0.62176728 0.6980682 0.57192993 0.69806808 0.52807134 0.69806802 0.82266289 0.61133504
		 0.75800854 0.61133498 0.68551725 0.61133504 0.61004323 0.61133504 0.53697097 0.61133504
		 0.4715209 0.61133504 0.87517506 0.52140105 0.79013264 0.52140111 0.69637555 0.52140099
		 0.59931117 0.52140105 0.50497723 0.52140099 0.41891035 0.52140105 0.92124301 0.42813337
		 0.81764483 0.42813343 0.7054289 0.42813337 0.59006047 0.42813337 0.47739846 0.42813349
		 0.37277508 0.42813337 0.9574545 0.3314358 0.83855152 0.33143574 0.71202213 0.33143562
		 0.58287936 0.33143574 0.45624834 0.33143562 0.33670956 0.33143562 0.97917211 0.23101099
		 0.84981197 0.23101102 0.71509445 0.23101102 0.57886577 0.23101099 0.44466108 0.23101099
		 0.31550825 0.23101105 0.97925955 0.12541787 0.84639496 0.12541787 0.71352988 0.12541787
		 0.58066529 0.1254179 0.44780061 0.12541787 0.31493592 0.12541787 0.64709824 0.8666904
		 0.97925955 0.015308872 0.84639502 0.015308931 0.71352994 0.015308931 0.58066529 0.015308931
		 0.31493589 0.015308902 0.4478004 0.015308917;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 55 ".vt[0:54]"  -1.2789989e-008 0.42916083 3.11180043 0.13261797 0.40815622 3.11180043
		 0.2522544 0.3471984 3.11180043 0.34719837 0.2522544 3.11180043 0.40815622 0.132618 3.11180043
		 0.4291608 -5.4043254e-016 3.11180043 -2.519136e-008 0.84528184 2.9692452 0.26120642 0.80391085 2.9692452
		 0.49684414 0.68384737 2.9692452 0.68384725 0.49684417 2.9692452 0.80391073 0.26120645 2.9692452
		 0.84528178 -5.1567472e-016 2.9692452 -3.6827302e-008 1.23571932 2.73647118 0.38185823 1.17523897 2.73647118
		 0.72633749 0.99971795 2.73647118 0.99971789 0.72633761 2.73647118 1.17523885 0.38185826 2.73647118
		 1.2357192 -4.7524838e-016 2.73647118 -4.7344269e-008 1.58861017 2.42055106 0.49090749 1.51085818 2.42055106
		 0.9337616 1.28521276 2.42055106 1.28521252 0.93376166 2.42055106 1.51085806 0.49090752 2.42055106
		 1.58861005 -4.2038187e-016 2.42055106 -5.6422707e-008 1.89323199 2.031083345 0.58504081 1.80057073 2.031083345
		 1.11281371 1.53165674 2.031083345 1.53165674 1.11281383 2.031083345 1.80057049 0.58504081 2.031083345
		 1.89323187 -3.5274227e-016 2.031083345 -6.3786757e-008 2.14032888 1.57990229 0.66139787 2.035573483 1.57990229
		 1.25805354 1.73156238 1.57990229 1.73156214 1.25805366 1.57990229 2.035573483 0.66139793 1.57990229
		 2.14032841 -2.7438478e-016 1.57990229 -6.9212689e-008 2.3223927 1.080716968 0.7176587 2.20872664 1.080716968
		 1.36506808 1.87885511 1.080716968 1.87885499 1.3650682 1.080716968 2.20872641 0.71765876 1.080716968
		 2.32239246 -1.8769026e-016 1.080716968 -7.2535627e-008 2.43389201 0.54869449 0.75211388 2.31476879 0.54869449
		 1.43060565 1.96905994 0.54869449 1.96905971 1.43060577 0.54869449 2.31476879 0.75211394 0.54869449
		 2.43389177 -9.5292861e-017 0.54869449 0 -5.4876956e-016 3.15980506 2.43389153 1.7945419e-017 0
		 2.31476879 0.75211394 0 1.96905971 1.43060577 0 1.43060565 1.96905994 0 0.75211388 2.31476903 0
		 -7.253562e-008 2.43389225 0;
	setAttr -s 99 ".ed[0:98]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 6 7 1 7 8 1
		 8 9 1 9 10 1 10 11 1 12 13 1 13 14 1 14 15 1 15 16 1 16 17 1 18 19 1 19 20 1 20 21 1
		 21 22 1 22 23 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1 30 31 1 31 32 1 32 33 1 33 34 1
		 34 35 1 36 37 1 37 38 1 38 39 1 39 40 1 40 41 1 42 43 1 43 44 1 44 45 1 45 46 1 46 47 1
		 0 6 0 1 7 1 2 8 1 3 9 1 4 10 1 5 11 0 6 12 0 7 13 1 8 14 1 9 15 1 10 16 1 11 17 0
		 12 18 0 13 19 1 14 20 1 15 21 1 16 22 1 17 23 0 18 24 0 19 25 1 20 26 1 21 27 1 22 28 1
		 23 29 0 24 30 0 25 31 1 26 32 1 27 33 1 28 34 1 29 35 0 30 36 0 31 37 1 32 38 1 33 39 1
		 34 40 1 35 41 0 36 42 0 37 43 1 38 44 1 39 45 1 40 46 1 41 47 0 42 54 0 43 53 1 44 52 1
		 45 51 1 46 50 1 47 49 0 48 0 0 48 1 1 48 2 1 48 3 1 48 4 1 48 5 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0;
	setAttr -s 45 -ch 175 ".fc[0:44]" -type "polyFaces" 
		f 4 0 41 -6 -41
		mu 0 4 0 1 7 6
		f 4 1 42 -7 -42
		mu 0 4 1 2 8 7
		f 4 2 43 -8 -43
		mu 0 4 2 3 9 8
		f 4 3 44 -9 -44
		mu 0 4 3 4 10 9
		f 4 4 45 -10 -45
		mu 0 4 4 5 11 10
		f 4 5 47 -11 -47
		mu 0 4 6 7 13 12
		f 4 6 48 -12 -48
		mu 0 4 7 8 14 13
		f 4 7 49 -13 -49
		mu 0 4 8 9 15 14
		f 4 8 50 -14 -50
		mu 0 4 9 10 16 15
		f 4 9 51 -15 -51
		mu 0 4 10 11 17 16
		f 4 10 53 -16 -53
		mu 0 4 12 13 19 18
		f 4 11 54 -17 -54
		mu 0 4 13 14 20 19
		f 4 12 55 -18 -55
		mu 0 4 14 15 21 20
		f 4 13 56 -19 -56
		mu 0 4 15 16 22 21
		f 4 14 57 -20 -57
		mu 0 4 16 17 23 22
		f 4 15 59 -21 -59
		mu 0 4 18 19 25 24
		f 4 16 60 -22 -60
		mu 0 4 19 20 26 25
		f 4 17 61 -23 -61
		mu 0 4 20 21 27 26
		f 4 18 62 -24 -62
		mu 0 4 21 22 28 27
		f 4 19 63 -25 -63
		mu 0 4 22 23 29 28
		f 4 20 65 -26 -65
		mu 0 4 24 25 31 30
		f 4 21 66 -27 -66
		mu 0 4 25 26 32 31
		f 4 22 67 -28 -67
		mu 0 4 26 27 33 32
		f 4 23 68 -29 -68
		mu 0 4 27 28 34 33
		f 4 24 69 -30 -69
		mu 0 4 28 29 35 34
		f 4 25 71 -31 -71
		mu 0 4 30 31 37 36
		f 4 26 72 -32 -72
		mu 0 4 31 32 38 37
		f 4 27 73 -33 -73
		mu 0 4 32 33 39 38
		f 4 28 74 -34 -74
		mu 0 4 33 34 40 39
		f 4 29 75 -35 -75
		mu 0 4 34 35 41 40
		f 4 30 77 -36 -77
		mu 0 4 36 37 43 42
		f 4 31 78 -37 -78
		mu 0 4 37 38 44 43
		f 4 32 79 -38 -79
		mu 0 4 38 39 45 44
		f 4 33 80 -39 -80
		mu 0 4 39 40 46 45
		f 4 34 81 -40 -81
		mu 0 4 40 41 47 46
		f 4 83 98 -83 35
		mu 0 4 43 50 49 42
		f 4 84 97 -84 36
		mu 0 4 44 51 50 43
		f 4 85 96 -85 37
		mu 0 4 45 52 51 44
		f 4 86 95 -86 38
		mu 0 4 46 54 52 45
		f 4 87 94 -87 39
		mu 0 4 47 53 54 46
		f 3 -1 -89 89
		mu 0 3 1 0 48
		f 3 -2 -90 90
		mu 0 3 2 1 48
		f 3 -3 -91 91
		mu 0 3 3 2 48
		f 3 -4 -92 92
		mu 0 3 4 3 48
		f 3 -5 -93 93
		mu 0 3 5 4 48;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere8" -p "Planet";
	setAttr ".s" -type "double3" -1 -1 1 ;
createNode transform -n "pSphere6" -p "Planet";
	setAttr ".s" -type "double3" 1 -1 -1 ;
createNode transform -n "pSphere5" -p "Planet";
	setAttr ".s" -type "double3" -1 -1 -1 ;
createNode transform -n "pSphere4" -p "Planet";
	setAttr ".s" -type "double3" 1 1 -1 ;
createNode transform -n "pSphere3" -p "Planet";
	setAttr ".s" -type "double3" -1 1 -1 ;
createNode transform -n "pSphere2" -p "Planet";
	setAttr ".s" -type "double3" -1 1 1 ;
createNode transform -n "pSphere1" -p "Planet";
createNode transform -n "pTorus1" -p "Planet";
	setAttr ".s" -type "double3" 1.0034766014168224 1.0034766014168224 1.0034766014168224 ;
createNode mesh -n "pTorusShape1" -p "pTorus1";
	setAttr -k off ".v";
	setAttr -s 2 ".iog";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.94014179706573486 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 105 ".uvst[0].uvsp[0:104]" -type "float2" -1.065515041 0.9908604
		 -0.90896338 0.9908604 -0.75241202 0.9908604 -0.59586042 0.9908604 -0.43930906 0.9908604
		 -0.28275752 0.9908604 -0.12620604 0.9908604 0.0303455 0.9908604 0.18689701 0.9908604
		 0.34344852 0.9908604 0.50000006 0.9908604 0.6565516 0.9908604 0.81310308 0.9908604
		 0.96965462 0.9908604 1.12620616 0.9908604 1.28275764 0.9908604 1.43930912 0.9908604
		 1.59586072 0.9908604 1.75241232 0.9908604 1.90896368 0.9908604 2.065515041 0.9908604
		 -1.065515041 0.96550119 -0.90896338 0.96550119 -0.75241202 0.96550119 -0.59586042
		 0.96550119 -0.43930906 0.96550119 -0.28275752 0.96550119 -0.12620604 0.96550119 0.0303455
		 0.96550119 0.18689701 0.96550119 0.34344852 0.96550119 0.50000006 0.96550119 0.6565516
		 0.96550119 0.81310308 0.96550119 0.96965462 0.96550119 1.12620616 0.96550119 1.28275764
		 0.96550119 1.43930912 0.96550119 1.59586072 0.96550119 1.75241232 0.96550119 1.90896368
		 0.96550119 2.065515041 0.96550119 -1.065515041 0.94014162 -0.90896338 0.94014162
		 -0.75241202 0.94014162 -0.59586042 0.94014162 -0.43930906 0.94014162 -0.28275752
		 0.94014162 -0.12620604 0.94014162 0.0303455 0.94014162 0.18689701 0.94014162 0.34344852
		 0.94014162 0.50000006 0.94014162 0.6565516 0.94014162 0.81310308 0.94014162 0.96965462
		 0.94014162 1.12620616 0.94014162 1.28275764 0.94014162 1.43930912 0.94014162 1.59586072
		 0.94014162 1.75241232 0.94014162 1.90896368 0.94014162 2.065515041 0.94014162 -1.065515041
		 0.9147824 -0.90896338 0.9147824 -0.75241202 0.9147824 -0.59586042 0.9147824 -0.43930906
		 0.9147824 -0.28275752 0.9147824 -0.12620604 0.9147824 0.0303455 0.9147824 0.18689701
		 0.9147824 0.34344852 0.9147824 0.50000006 0.9147824 0.6565516 0.9147824 0.81310308
		 0.9147824 0.96965462 0.9147824 1.12620616 0.9147824 1.28275764 0.9147824 1.43930912
		 0.9147824 1.59586072 0.9147824 1.75241232 0.9147824 1.90896368 0.9147824 2.065515041
		 0.9147824 -1.065515041 0.88942319 -0.90896338 0.88942319 -0.75241202 0.88942319 -0.59586042
		 0.88942319 -0.43930906 0.88942319 -0.28275752 0.88942319 -0.12620604 0.88942319 0.0303455
		 0.88942319 0.18689701 0.88942319 0.34344852 0.88942319 0.50000006 0.88942319 0.6565516
		 0.88942319 0.81310308 0.88942319 0.96965462 0.88942319 1.12620616 0.88942319 1.28275764
		 0.88942319 1.43930912 0.88942319 1.59586072 0.88942319 1.75241232 0.88942319 1.90896368
		 0.88942319 2.065515041 0.88942319;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 80 ".vt[0:79]"  2.34951091 0.76340234 -0.32025418 1.99861348 1.45207751 -0.32025418
		 1.45207751 1.99861312 -0.32025418 0.76340222 2.34951067 -0.32025418 0 2.47042155 -0.32025418
		 -0.76340222 2.34951043 -0.32025418 -1.45207739 1.99861288 -0.32025418 -1.99861252 1.45207703 -0.32025418
		 -2.34950995 0.7634021 -0.32025418 -2.47042108 8.2837713e-018 -0.32025418 -2.34950995 -0.7634021 -0.32025418
		 -1.99861252 -1.45207703 -0.32025418 -1.45207703 -1.9986124 -0.32025418 -0.7634021 -2.34950995 -0.32025418
		 -7.3624264e-008 -2.47042084 -0.32025418 0.76340181 -2.34950948 -0.32025418 1.45207667 -1.99861217 -0.32025418
		 1.99861205 -1.45207667 -0.32025418 2.34950948 -0.76340193 -0.32025418 2.47042036 8.2837713e-018 -0.32025418
		 2.48628807 0.80784392 -0.32025418 2.11496305 1.53661036 -0.32025418 1.53661036 2.11496282 -0.32025418
		 0.80784386 2.48628783 -0.32025418 0 2.61423731 -0.32025418 -0.80784386 2.48628759 -0.32025418
		 -1.53661025 2.11496258 -0.32025418 -2.11496234 1.53661001 -0.32025418 -2.48628712 0.80784363 -0.32025418
		 -2.61423707 8.2837705e-018 -0.32025418 -2.48628712 -0.80784363 -0.32025418 -2.1149621 -1.53661001 -0.32025418
		 -1.53661001 -2.1149621 -0.32025418 -0.80784363 -2.48628688 -0.32025418 -7.7910315e-008 -2.61423683 -0.32025418
		 0.80784339 -2.48628664 -0.32025418 1.53660953 -2.11496186 -0.32025418 2.11496162 -1.53660965 -0.32025418
		 2.48628664 -0.80784345 -0.32025418 2.61423635 8.2837705e-018 -0.32025418 2.48628807 0.80784392 -0.39486775
		 2.11496305 1.53661036 -0.39486775 1.53661036 2.11496282 -0.39486775 0.80784386 2.48628783 -0.39486775
		 0 2.61423731 -0.39486775 -0.80784386 2.48628759 -0.39486775 -1.53661025 2.11496258 -0.39486775
		 -2.11496234 1.53661001 -0.39486775 -2.48628712 0.80784363 -0.39486775 -2.61423707 -8.2837729e-018 -0.39486775
		 -2.48628712 -0.80784363 -0.39486775 -2.1149621 -1.53661001 -0.39486775 -1.53661001 -2.1149621 -0.39486775
		 -0.80784363 -2.48628688 -0.39486775 -7.7910315e-008 -2.61423683 -0.39486775 0.80784339 -2.48628664 -0.39486775
		 1.53660953 -2.11496186 -0.39486775 2.11496162 -1.53660965 -0.39486775 2.48628664 -0.80784345 -0.39486775
		 2.61423635 -8.2837729e-018 -0.39486775 2.34951091 0.76340234 -0.39486775 1.99861348 1.45207751 -0.39486775
		 1.45207751 1.99861312 -0.39486775 0.76340222 2.34951067 -0.39486775 0 2.47042155 -0.39486775
		 -0.76340222 2.34951043 -0.39486775 -1.45207739 1.99861288 -0.39486775 -1.99861252 1.45207703 -0.39486775
		 -2.34950995 0.7634021 -0.39486775 -2.47042108 -8.2837688e-018 -0.39486775 -2.34950995 -0.7634021 -0.39486775
		 -1.99861252 -1.45207703 -0.39486775 -1.45207703 -1.9986124 -0.39486775 -0.7634021 -2.34950995 -0.39486775
		 -7.3624264e-008 -2.47042084 -0.39486775 0.76340181 -2.34950948 -0.39486775 1.45207667 -1.99861217 -0.39486775
		 1.99861205 -1.45207667 -0.39486775 2.34950948 -0.76340193 -0.39486775 2.47042036 -8.2837688e-018 -0.39486775;
	setAttr -s 160 ".ed[0:159]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 0 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0
		 40 41 0 41 42 0 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 40 0 60 61 0 61 62 0
		 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 70 0 70 71 0 71 72 0 72 73 0
		 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 60 0 0 20 1 1 21 1 2 22 1 3 23 1
		 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1 12 32 1 13 33 1 14 34 1
		 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 20 40 1 21 41 1 22 42 1 23 43 1 24 44 1 25 45 1
		 26 46 1 27 47 1 28 48 1 29 49 1 30 50 1 31 51 1 32 52 1 33 53 1 34 54 1 35 55 1 36 56 1
		 37 57 1 38 58 1 39 59 1 40 60 1 41 61 1 42 62 1 43 63 1 44 64 1 45 65 1 46 66 1 47 67 1
		 48 68 1 49 69 1 50 70 1 51 71 1 52 72 1 53 73 1 54 74 1 55 75 1 56 76 1 57 77 1 58 78 1
		 59 79 1 60 0 1 61 1 1 62 2 1 63 3 1 64 4 1 65 5 1 66 6 1 67 7 1 68 8 1 69 9 1 70 10 1
		 71 11 1 72 12 1 73 13 1 74 14 1 75 15 1 76 16 1 77 17 1 78 18 1 79 19 1;
	setAttr -s 80 -ch 320 ".fc[0:79]" -type "polyFaces" 
		f 4 -1 80 20 -82
		mu 0 4 1 0 21 22
		f 4 -2 81 21 -83
		mu 0 4 2 1 22 23
		f 4 -3 82 22 -84
		mu 0 4 3 2 23 24
		f 4 -4 83 23 -85
		mu 0 4 4 3 24 25
		f 4 -5 84 24 -86
		mu 0 4 5 4 25 26
		f 4 -6 85 25 -87
		mu 0 4 6 5 26 27
		f 4 -7 86 26 -88
		mu 0 4 7 6 27 28
		f 4 -8 87 27 -89
		mu 0 4 8 7 28 29
		f 4 -9 88 28 -90
		mu 0 4 9 8 29 30
		f 4 -10 89 29 -91
		mu 0 4 10 9 30 31
		f 4 -11 90 30 -92
		mu 0 4 11 10 31 32
		f 4 -12 91 31 -93
		mu 0 4 12 11 32 33
		f 4 -13 92 32 -94
		mu 0 4 13 12 33 34
		f 4 -14 93 33 -95
		mu 0 4 14 13 34 35
		f 4 -15 94 34 -96
		mu 0 4 15 14 35 36
		f 4 -16 95 35 -97
		mu 0 4 16 15 36 37
		f 4 -17 96 36 -98
		mu 0 4 17 16 37 38
		f 4 -18 97 37 -99
		mu 0 4 18 17 38 39
		f 4 -19 98 38 -100
		mu 0 4 19 18 39 40
		f 4 -20 99 39 -81
		mu 0 4 20 19 40 41
		f 4 -21 100 40 -102
		mu 0 4 22 21 42 43
		f 4 -22 101 41 -103
		mu 0 4 23 22 43 44
		f 4 -23 102 42 -104
		mu 0 4 24 23 44 45
		f 4 -24 103 43 -105
		mu 0 4 25 24 45 46
		f 4 -25 104 44 -106
		mu 0 4 26 25 46 47
		f 4 -26 105 45 -107
		mu 0 4 27 26 47 48
		f 4 -27 106 46 -108
		mu 0 4 28 27 48 49
		f 4 -28 107 47 -109
		mu 0 4 29 28 49 50
		f 4 -29 108 48 -110
		mu 0 4 30 29 50 51
		f 4 -30 109 49 -111
		mu 0 4 31 30 51 52
		f 4 -31 110 50 -112
		mu 0 4 32 31 52 53
		f 4 -32 111 51 -113
		mu 0 4 33 32 53 54
		f 4 -33 112 52 -114
		mu 0 4 34 33 54 55
		f 4 -34 113 53 -115
		mu 0 4 35 34 55 56
		f 4 -35 114 54 -116
		mu 0 4 36 35 56 57
		f 4 -36 115 55 -117
		mu 0 4 37 36 57 58
		f 4 -37 116 56 -118
		mu 0 4 38 37 58 59
		f 4 -38 117 57 -119
		mu 0 4 39 38 59 60
		f 4 -39 118 58 -120
		mu 0 4 40 39 60 61
		f 4 -40 119 59 -101
		mu 0 4 41 40 61 62
		f 4 -41 120 60 -122
		mu 0 4 43 42 63 64
		f 4 -42 121 61 -123
		mu 0 4 44 43 64 65
		f 4 -43 122 62 -124
		mu 0 4 45 44 65 66
		f 4 -44 123 63 -125
		mu 0 4 46 45 66 67
		f 4 -45 124 64 -126
		mu 0 4 47 46 67 68
		f 4 -46 125 65 -127
		mu 0 4 48 47 68 69
		f 4 -47 126 66 -128
		mu 0 4 49 48 69 70
		f 4 -48 127 67 -129
		mu 0 4 50 49 70 71
		f 4 -49 128 68 -130
		mu 0 4 51 50 71 72
		f 4 -50 129 69 -131
		mu 0 4 52 51 72 73
		f 4 -51 130 70 -132
		mu 0 4 53 52 73 74
		f 4 -52 131 71 -133
		mu 0 4 54 53 74 75
		f 4 -53 132 72 -134
		mu 0 4 55 54 75 76
		f 4 -54 133 73 -135
		mu 0 4 56 55 76 77
		f 4 -55 134 74 -136
		mu 0 4 57 56 77 78
		f 4 -56 135 75 -137
		mu 0 4 58 57 78 79
		f 4 -57 136 76 -138
		mu 0 4 59 58 79 80
		f 4 -58 137 77 -139
		mu 0 4 60 59 80 81
		f 4 -59 138 78 -140
		mu 0 4 61 60 81 82
		f 4 -60 139 79 -121
		mu 0 4 62 61 82 83
		f 4 -61 140 0 -142
		mu 0 4 64 63 84 85
		f 4 -62 141 1 -143
		mu 0 4 65 64 85 86
		f 4 -63 142 2 -144
		mu 0 4 66 65 86 87
		f 4 -64 143 3 -145
		mu 0 4 67 66 87 88
		f 4 -65 144 4 -146
		mu 0 4 68 67 88 89
		f 4 -66 145 5 -147
		mu 0 4 69 68 89 90
		f 4 -67 146 6 -148
		mu 0 4 70 69 90 91
		f 4 -68 147 7 -149
		mu 0 4 71 70 91 92
		f 4 -69 148 8 -150
		mu 0 4 72 71 92 93
		f 4 -70 149 9 -151
		mu 0 4 73 72 93 94
		f 4 -71 150 10 -152
		mu 0 4 74 73 94 95
		f 4 -72 151 11 -153
		mu 0 4 75 74 95 96
		f 4 -73 152 12 -154
		mu 0 4 76 75 96 97
		f 4 -74 153 13 -155
		mu 0 4 77 76 97 98
		f 4 -75 154 14 -156
		mu 0 4 78 77 98 99
		f 4 -76 155 15 -157
		mu 0 4 79 78 99 100
		f 4 -77 156 16 -158
		mu 0 4 80 79 100 101
		f 4 -78 157 17 -159
		mu 0 4 81 80 101 102
		f 4 -79 158 18 -160
		mu 0 4 82 81 102 103
		f 4 -80 159 19 -141
		mu 0 4 83 82 103 104;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pTorus2" -p "Planet";
	setAttr ".s" -type "double3" 1.0034766014168224 1.0034766014168224 -1.0034766014168224 ;
createNode geometryVarGroup -n "snapshot1Group" -p "Planet";
	setAttr ".mc" 21;
	setAttr -s 21 ".l";
createNode transform -n "transform1_1" -p "snapshot1Group";
createNode mesh -n "transform1_Shape1" -p "transform1_1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_2" -p "snapshot1Group";
createNode mesh -n "transform1_Shape2" -p "transform1_2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_3" -p "snapshot1Group";
createNode mesh -n "transform1_Shape3" -p "transform1_3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_4" -p "snapshot1Group";
createNode mesh -n "transform1_Shape4" -p "transform1_4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_5" -p "snapshot1Group";
createNode mesh -n "transform1_Shape5" -p "transform1_5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_6" -p "snapshot1Group";
createNode mesh -n "transform1_Shape6" -p "transform1_6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_7" -p "snapshot1Group";
createNode mesh -n "transform1_Shape7" -p "transform1_7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_8" -p "snapshot1Group";
createNode mesh -n "transform1_Shape8" -p "transform1_8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_9" -p "snapshot1Group";
createNode mesh -n "transform1_Shape9" -p "transform1_9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_10" -p "snapshot1Group";
createNode mesh -n "transform1_Shape10" -p "transform1_10";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_11" -p "snapshot1Group";
createNode mesh -n "transform1_Shape11" -p "transform1_11";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_12" -p "snapshot1Group";
createNode mesh -n "transform1_Shape12" -p "transform1_12";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_13" -p "snapshot1Group";
createNode mesh -n "transform1_Shape13" -p "transform1_13";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_14" -p "snapshot1Group";
createNode mesh -n "transform1_Shape14" -p "transform1_14";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_15" -p "snapshot1Group";
createNode mesh -n "transform1_Shape15" -p "transform1_15";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_16" -p "snapshot1Group";
createNode mesh -n "transform1_Shape16" -p "transform1_16";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_17" -p "snapshot1Group";
createNode mesh -n "transform1_Shape17" -p "transform1_17";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_18" -p "snapshot1Group";
createNode mesh -n "transform1_Shape18" -p "transform1_18";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_19" -p "snapshot1Group";
createNode mesh -n "transform1_Shape19" -p "transform1_19";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_20" -p "snapshot1Group";
createNode mesh -n "transform1_Shape20" -p "transform1_20";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.91873574256896973 0.76971590518951416 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform1_21" -p "snapshot1Group";
createNode mesh -n "transform1_Shape21" -p "transform1_21";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "Wood" -p "LVL_Planet_";
	setAttr ".rp" -type "double3" 1.0658141036401502e-016 2.4005441959286062 -3.8603694930225807e-016 ;
	setAttr ".sp" -type "double3" 1.0658141036401502e-016 2.4005441959286062 -3.8603694930225807e-016 ;
	setAttr ".smd" 7;
createNode mesh -n "WoodShape" -p "Wood";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49734538793563843 0.50131960585713387 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.039696805 0.011707073
		 0.11176451 0.011707103 0.11176451 0.040561963 0.039696805 0.040561993 0.16286302
		 0.096070923 0.20387109 0.096069135 0.11176451 0.38968518 0.039696805 0.38968518 0.20387048
		 0.015864063 0.16286299 0.015863217 0.11176451 0.41853991 0.03969688 0.41853997 0.14061949
		 0.040561993 0.14061937 0.38968518 0.2833291 0.096068352 0.32177928 0.096067995 0.010841811
		 0.38968518 0.010841811 0.040561993 0.32177868 0.01586286 0.2833285 0.015863057 0.2422637
		 0.13451926 0.24226367 0.096068956 0.28327197 0.096068777 0.283272 0.13451907 0.24232104
		 0.13451889 0.24232104 0.096068665 0.16280517 0.015863225 0.28332925 0.13451877 0.20381281
		 0.015863992 0.24226317 0.015863463 0.1628052 0.096070923 0.20381325 0.096069373 0.28327134
		 0.01586305 0.32172224 0.096068352 0.24232045 0.015863389 0.3217217 0.01586286;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  0.10956535 2.39326262 -0.53735131 -0.10956535 2.39326262 -0.53735131
		 0.10956535 2.48099947 -0.53735131 -0.10956535 2.48099947 -0.53735131 0.10956535 2.48099947 0.52419847
		 -0.10956535 2.48099947 0.52419847 0.10956535 2.39326262 0.52419847 -0.10956535 2.39326262 0.52419847
		 0.024360277 2.4753511 0.45482141 -0.00078665733 2.4753511 0.43271819 -0.024360254 2.4753511 0.45953801
		 0.00078668335 2.4753511 0.48164111 0.024360277 2.5451889 0.45482141 -0.00078665733 2.5451889 0.43271819
		 -0.024360254 2.5451889 0.45953801 0.00078668335 2.5451889 0.48164111 0.024360277 2.5451889 -0.46734703
		 -0.00078665733 2.5451889 -0.48945016 -0.024360254 2.5451889 -0.46263042 0.00078668335 2.5451889 -0.44052723
		 0.024360277 2.4753511 -0.46734703 -0.00078665733 2.4753511 -0.48945016 -0.024360254 2.4753511 -0.46263042
		 0.00078668335 2.4753511 -0.44052723;
	setAttr -s 36 ".ed[0:35]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 9 10 0 11 10 0 8 11 0 8 12 1 9 13 1 12 13 1 10 14 1
		 13 14 1 11 15 1 15 14 1 12 15 1 16 17 1 17 18 1 19 18 1 16 19 1 20 21 0 21 17 1 20 16 1
		 21 22 0 22 18 1 23 22 0 23 19 1 20 23 0;
	setAttr -s 15 -ch 60 ".fc[0:14]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 2 3
		f 4 1 7 -3 -7
		mu 0 4 3 2 6 7
		f 4 2 9 -4 -9
		mu 0 4 7 6 10 11
		f 4 -12 -10 -8 -6
		mu 0 4 12 13 6 2
		f 4 10 4 6 8
		mu 0 4 16 17 3 7
		f 4 18 20 -23 -24
		mu 0 4 20 21 22 23
		f 4 12 17 -19 -17
		mu 0 4 28 29 21 31
		f 4 13 19 -21 -18
		mu 0 4 29 32 22 21
		f 4 -15 21 22 -20
		mu 0 4 32 35 33 22
		f 4 -16 16 23 -22
		mu 0 4 26 28 31 30
		f 4 24 25 -27 -28
		mu 0 4 24 25 14 27
		f 4 28 29 -25 -31
		mu 0 4 8 34 25 5
		f 4 31 32 -26 -30
		mu 0 4 34 19 14 25
		f 4 -34 34 26 -33
		mu 0 4 19 18 15 14
		f 4 -36 30 27 -35
		mu 0 4 9 8 5 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Circle";
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.97442956593227137 0.97442956593227137 0.97442956593227137 ;
createNode nurbsCurve -n "CircleShape" -p "Circle";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.9304569604218693 1.1820639687361839e-016 -1.9304569604218664
		-3.1146930349678422e-016 1.6716908961792787e-016 -2.730078415006147
		-1.9304569604218678 1.1820639687361844e-016 -1.9304569604218678
		-2.730078415006147 4.8441419930865071e-032 -7.9110842349960612e-016
		-1.9304569604218682 -1.1820639687361842e-016 1.9304569604218671
		-8.2262590697479373e-016 -1.671690896179279e-016 2.7300784150061475
		1.9304569604218664 -1.1820639687361847e-016 1.930456960421868
		2.730078415006147 -8.9786844136293822e-032 1.4663304423578607e-015
		1.9304569604218693 1.1820639687361839e-016 -1.9304569604218664
		-3.1146930349678422e-016 1.6716908961792787e-016 -2.730078415006147
		-1.9304569604218678 1.1820639687361844e-016 -1.9304569604218678
		;
createNode transform -n "positionMarker5" -p "CircleShape";
createNode positionMarker -n "positionMarkerShape5" -p "positionMarker5";
	setAttr -k off ".v";
	setAttr ".uwo" yes;
createNode transform -n "positionMarker6" -p "CircleShape";
createNode positionMarker -n "positionMarkerShape6" -p "positionMarker6";
	setAttr -k off ".v";
	setAttr ".uwo" yes;
	setAttr ".lp" -type "double3" 0.01 0 0 ;
	setAttr ".t" 20;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1" "pSphere1" ;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1" "pSphere2" ;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1" "pSphere3" ;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1" "pSphere4" ;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1" "pSphere5" ;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1" "pSphere6" ;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1" "pSphere8" ;
parent -s -nc -r -add "|LVL_Planet_|A|Planet|pTorus1|pTorusShape1" "pTorus2" ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 20 -ast 0 -aet 20 ";
	setAttr ".st" 6;
createNode polyMapSewMove -n "polyMapSewMove1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[6]";
createNode shadingEngine -n "SG_Checker_256SG";
	setAttr ".ihi" 0;
	setAttr -s 32 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode place2dTexture -n "P2dTex_Checker_256";
createNode file -n "Tex_Checker_256";
	setAttr ".ftn" -type "string" "G:/ninjaegg-nngj2-bteam/3D//SourcesImages/Checker_256.jpg";
createNode lambert -n "Mat_Checker_256";
	setAttr ".dc" 1;
createNode motionPath -n "motionPath1";
	setAttr -s 2 ".pmt";
	setAttr -s 2 ".pmt";
	setAttr ".f" yes;
	setAttr ".fa" 0;
	setAttr ".ua" 1;
	setAttr ".fm" yes;
createNode addDoubleLinear -n "addDoubleLinear3";
createNode addDoubleLinear -n "addDoubleLinear1";
createNode addDoubleLinear -n "addDoubleLinear2";
createNode animCurveTL -n "motionPath1_uValue";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 20 0.01;
createNode objectSet -n "modelPanel4ViewSelectedSet";
	setAttr ".ihi" 0;
createNode polyTweakUV -n "polyTweakUV1";
	setAttr ".uopa" yes;
	setAttr -s 25 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" 0.7349928 0.59421581 ;
	setAttr ".uvtk[5]" -type "float2" 0.69398648 0.63522565 ;
	setAttr ".uvtk[8]" -type "float2" 0.77419215 0.71543014 ;
	setAttr ".uvtk[9]" -type "float2" 0.81520039 0.67442334 ;
	setAttr ".uvtk[14]" -type "float2" 0.61452931 0.71468443 ;
	setAttr ".uvtk[15]" -type "float2" 0.57607955 0.75313491 ;
	setAttr ".uvtk[18]" -type "float2" 0.65628523 0.83333951 ;
	setAttr ".uvtk[19]" -type "float2" 0.69473511 0.79488921 ;
	setAttr ".uvtk[20]" -type "float2" 0.61714381 0.63516814 ;
	setAttr ".uvtk[21]" -type "float2" 0.65559405 0.67361838 ;
	setAttr ".uvtk[22]" -type "float2" 0.61458588 0.71462685 ;
	setAttr ".uvtk[23]" -type "float2" 0.57613558 0.67617679 ;
	setAttr ".uvtk[24]" -type "float2" 0.61708695 0.63522589 ;
	setAttr ".uvtk[25]" -type "float2" 0.65553695 0.67367619 ;
	setAttr ".uvtk[26]" -type "float2" 0.81525809 0.67436576 ;
	setAttr ".uvtk[27]" -type "float2" 0.57607877 0.6762343 ;
	setAttr ".uvtk[28]" -type "float2" 0.77424985 0.71537244 ;
	setAttr ".uvtk[29]" -type "float2" 0.73579997 0.75382352 ;
	setAttr ".uvtk[30]" -type "float2" 0.73505062 0.59415799 ;
	setAttr ".uvtk[31]" -type "float2" 0.69404405 0.63516748 ;
	setAttr ".uvtk[32]" -type "float2" 0.69479233 0.79483187 ;
	setAttr ".uvtk[33]" -type "float2" 0.57613617 0.75307757 ;
	setAttr ".uvtk[34]" -type "float2" 0.73574275 0.75388068 ;
	setAttr ".uvtk[35]" -type "float2" 0.65634221 0.83328253 ;
createNode motionTrail -n "snapshot1";
	setAttr ".e" 20;
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 3 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "snapshot1.out" "snapshot1Group.cr";
connectAttr "snapshot1Group.l[0]" "transform1_Shape1.i";
connectAttr "snapshot1Group.l[1]" "transform1_Shape2.i";
connectAttr "snapshot1Group.l[2]" "transform1_Shape3.i";
connectAttr "snapshot1Group.l[3]" "transform1_Shape4.i";
connectAttr "snapshot1Group.l[4]" "transform1_Shape5.i";
connectAttr "snapshot1Group.l[5]" "transform1_Shape6.i";
connectAttr "snapshot1Group.l[6]" "transform1_Shape7.i";
connectAttr "snapshot1Group.l[7]" "transform1_Shape8.i";
connectAttr "snapshot1Group.l[8]" "transform1_Shape9.i";
connectAttr "snapshot1Group.l[9]" "transform1_Shape10.i";
connectAttr "snapshot1Group.l[10]" "transform1_Shape11.i";
connectAttr "snapshot1Group.l[11]" "transform1_Shape12.i";
connectAttr "snapshot1Group.l[12]" "transform1_Shape13.i";
connectAttr "snapshot1Group.l[13]" "transform1_Shape14.i";
connectAttr "snapshot1Group.l[14]" "transform1_Shape15.i";
connectAttr "snapshot1Group.l[15]" "transform1_Shape16.i";
connectAttr "snapshot1Group.l[16]" "transform1_Shape17.i";
connectAttr "snapshot1Group.l[17]" "transform1_Shape18.i";
connectAttr "snapshot1Group.l[18]" "transform1_Shape19.i";
connectAttr "polyTweakUV1.out" "transform1_Shape20.i";
connectAttr "polyTweakUV1.uvtk[0]" "transform1_Shape20.uvst[0].uvtw";
connectAttr "snapshot1Group.l[20]" "transform1_Shape21.i";
connectAttr "addDoubleLinear1.o" "Wood.tx";
connectAttr "addDoubleLinear2.o" "Wood.ty";
connectAttr "addDoubleLinear3.o" "Wood.tz";
connectAttr "motionPath1.msg" "Wood.sml";
connectAttr "motionPath1.rx" "Wood.rx";
connectAttr "motionPath1.ry" "Wood.ry";
connectAttr "motionPath1.rz" "Wood.rz";
connectAttr "motionPath1.ro" "Wood.ro";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "SG_Checker_256SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "SG_Checker_256SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Mat_Checker_256.oc" "SG_Checker_256SG.ss";
connectAttr "|LVL_Planet_|A|Planet|pSphere1|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "WoodShape.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "|LVL_Planet_|A|Planet|pSphere2|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "|LVL_Planet_|A|Planet|pSphere3|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "|LVL_Planet_|A|Planet|pSphere4|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "|LVL_Planet_|A|Planet|pSphere5|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "|LVL_Planet_|A|Planet|pSphere6|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "|LVL_Planet_|A|Planet|pSphere7|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "|LVL_Planet_|A|Planet|pSphere8|pSphereShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "|LVL_Planet_|A|Planet|pTorus1|pTorusShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "transform1_Shape21.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape20.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape19.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape18.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape17.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape16.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape15.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape14.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape13.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape12.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape11.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape10.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape9.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape8.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape7.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape6.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape5.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape4.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape3.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape2.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "transform1_Shape1.iog" "SG_Checker_256SG.dsm" -na;
connectAttr "|LVL_Planet_|A|Planet|pTorus2|pTorusShape1.iog" "SG_Checker_256SG.dsm"
		 -na;
connectAttr "SG_Checker_256SG.msg" "materialInfo1.sg";
connectAttr "Mat_Checker_256.msg" "materialInfo1.m";
connectAttr "Tex_Checker_256.msg" "materialInfo1.t" -na;
connectAttr "P2dTex_Checker_256.o" "Tex_Checker_256.uv";
connectAttr "P2dTex_Checker_256.ofs" "Tex_Checker_256.fs";
connectAttr "P2dTex_Checker_256.c" "Tex_Checker_256.c";
connectAttr "P2dTex_Checker_256.tf" "Tex_Checker_256.tf";
connectAttr "P2dTex_Checker_256.rf" "Tex_Checker_256.rf";
connectAttr "P2dTex_Checker_256.mu" "Tex_Checker_256.mu";
connectAttr "P2dTex_Checker_256.mv" "Tex_Checker_256.mv";
connectAttr "P2dTex_Checker_256.s" "Tex_Checker_256.s";
connectAttr "P2dTex_Checker_256.wu" "Tex_Checker_256.wu";
connectAttr "P2dTex_Checker_256.wv" "Tex_Checker_256.wv";
connectAttr "P2dTex_Checker_256.re" "Tex_Checker_256.re";
connectAttr "P2dTex_Checker_256.vt1" "Tex_Checker_256.vt1";
connectAttr "P2dTex_Checker_256.vt2" "Tex_Checker_256.vt2";
connectAttr "P2dTex_Checker_256.vt3" "Tex_Checker_256.vt3";
connectAttr "P2dTex_Checker_256.vc1" "Tex_Checker_256.vc1";
connectAttr "P2dTex_Checker_256.n" "Tex_Checker_256.n";
connectAttr "P2dTex_Checker_256.of" "Tex_Checker_256.of";
connectAttr "P2dTex_Checker_256.r" "Tex_Checker_256.ro";
connectAttr "Tex_Checker_256.oc" "Mat_Checker_256.c";
connectAttr "motionPath1_uValue.o" "motionPath1.u";
connectAttr "CircleShape.ws" "motionPath1.gp";
connectAttr "positionMarkerShape5.t" "motionPath1.pmt[2]";
connectAttr "positionMarkerShape6.t" "motionPath1.pmt[3]";
connectAttr "Wood.tmrz" "addDoubleLinear3.i1";
connectAttr "motionPath1.zc" "addDoubleLinear3.i2";
connectAttr "Wood.tmrx" "addDoubleLinear1.i1";
connectAttr "motionPath1.xc" "addDoubleLinear1.i2";
connectAttr "Wood.tmry" "addDoubleLinear2.i1";
connectAttr "motionPath1.yc" "addDoubleLinear2.i2";
connectAttr "A.iog" "modelPanel4ViewSelectedSet.dsm" -na;
connectAttr "snapshot1Group.l[19]" "polyTweakUV1.ip";
connectAttr "WoodShape.w" "snapshot1.in";
connectAttr "SG_Checker_256SG.pa" ":renderPartition.st" -na;
connectAttr "Mat_Checker_256.msg" ":defaultShaderList1.s" -na;
connectAttr "P2dTex_Checker_256.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "Tex_Checker_256.msg" ":defaultTextureList1.tx" -na;
dataStructure -fmt "raw" -as "name=externalContentTable:string=node:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
applyMetadata -fmt "raw" -v "channel\nname externalContentTable\nstream\nname v1.0\nindexType numeric\nstructure externalContentTable\n0\n\"Tex_Checker_256\" \"fileTextureName\" \"G:/ninjaegg-nngj2-bteam/3D/SourcesImages/Checker_256.jpg\" 1744881837 \"G:/ninjaegg-nngj2-bteam/3D/SourcesImages/Checker_256.jpg\" \"sourceImages\"\nendStream\nendChannel\nendAssociations\n" 
		-scn;
// End of LVL_Planet_A.ma
